package fr.afpa.braderie.LabBraderie_Spring;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fr.afpa.braderie.LabBraderie_Spring.beans.Article;
import fr.afpa.braderie.LabBraderie_Spring.beans.Panier;
import fr.afpa.braderie.LabBraderie_Spring.beans.User;
import fr.afpa.braderie.LabBraderie_Spring.services.ServiceArticle;
import fr.afpa.braderie.LabBraderie_Spring.services.ServicePanier;
import fr.afpa.braderie.LabBraderie_Spring.services.ServiceUser;


@SpringBootTest
class AppTests {

	@Autowired
	ServiceArticle serviceArticle;
	
	@Autowired
	ServicePanier servicePanier;
	
	@Autowired
	ServiceUser serviceUser;
	
	private Article article;
	private User user;
	private Boolean presOrNot = true;

	@Test
	void contextLoads() {
	}
	
	@Test
	public void addBouchonPourTest() {
	
		user = serviceUser.getByID(1).get();
		article = serviceArticle.getByID(5).get();
		if(servicePanier.findByUserAndArticle(user, article) == null) {
			Panier panier = new Panier(user, article, 2);
			servicePanier.add(panier);
			presOrNot = false;
		}
		
	}

	@Test
	public void testGetByIDArticle(){
		
		Article article = new Article();
		article.setIdarticle(5);
		article.setDescription("Montre");
		article.setMarque("Rolex");
		article.setPrixunitaire(200.00);
		
		assertEquals(serviceArticle.getByID(5).get().getMarque(), article.getMarque());
	}
	
	@Test
	public void testFindByUserAndArticle(){

		article = serviceArticle.getByID(5).get();
		user = serviceUser.getByID(1).get();

		assertEquals(servicePanier.findByUserAndArticle(user, article).getArticle().getMarque(), article.getMarque());
	}
	
	@Test
	public void testFindByUserAndArticleIsNull(){
		
		article = serviceArticle.getByID(2).get();
		user = serviceUser.getByID(1).get();

		assertNull(servicePanier.findByUserAndArticle(user, article));
	}
	
	@Test
	public void testAdd(){
		
		user = serviceUser.getByID(1).get();
		article = serviceArticle.getByID(5).get();
		Panier rolex = servicePanier.findByUserAndArticle(user, article);

		if(servicePanier.findByUserAndArticle(user, article) != null){
			servicePanier.delete(rolex.getIdpanier());
			servicePanier.add(rolex);
		}
		else {
			servicePanier.add(rolex);

		}
		
		assertEquals(rolex.getArticle().getMarque(), article.getMarque());
	}
	
	@Test
	public void testDelete(){
		
		user = serviceUser.getByID(1).get();
		article = serviceArticle.getByID(5).get();
		Panier rolex = servicePanier.findByUserAndArticle(user, article);

		if(rolex != null){
			servicePanier.delete(rolex.getIdpanier());
			assertFalse(servicePanier.getByID(rolex.getIdpanier()).isPresent());
			servicePanier.add(rolex);
		}
		else {
			servicePanier.add(rolex);
			servicePanier.delete(rolex.getIdpanier());
			assertFalse(servicePanier.getByID(rolex.getIdpanier()).isPresent());
		}

	}
	

	@Test
	public void testUpdateQuantite(){
		
		user = serviceUser.getByID(1).get();
		article = serviceArticle.getByID(5).get();
		Panier rolex = servicePanier.findByUserAndArticle(user, article);
		Integer oldQuantite = rolex.getQuantite();
		Integer ajoutQuantite = 5;
		
		if(servicePanier.findByUserAndArticle(user, article) != null){
			servicePanier.updateQuantite(rolex.getIdpanier(), ajoutQuantite);
		}
		else {
			servicePanier.add(rolex);
			servicePanier.updateQuantite(rolex.getIdpanier(), ajoutQuantite);

		}
		assertEquals(servicePanier.findByUserAndArticle(user, article).getQuantite(), oldQuantite+ajoutQuantite);
		rolex.setQuantite(oldQuantite);
	}
	
	@Test
	public void deleteBouchonPourTest() {
		
		if(presOrNot == false) {
			servicePanier.delete(servicePanier.findByUserAndArticle(user, article).getIdpanier());
		}		
	}
	
}
