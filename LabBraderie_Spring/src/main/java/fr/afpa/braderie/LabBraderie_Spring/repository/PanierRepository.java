package fr.afpa.braderie.LabBraderie_Spring.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import fr.afpa.braderie.LabBraderie_Spring.beans.Article;
import fr.afpa.braderie.LabBraderie_Spring.beans.Panier;
import fr.afpa.braderie.LabBraderie_Spring.beans.User;

@Repository
public interface PanierRepository extends CrudRepository <Panier, Integer> {

	/**
	 * Spring Query qui retourne la liste d'entrées du panier pour un utilisateur donné
	 * @param user
	 * @return 
	 */
	public Iterable<Panier> findByUser(User user);
	
	/**
	 * Spring Query qui retourne une entrée de la table t_panier correspondant à un user avec un article donné. 
	 * @param user
	 * @param article
	 * @return panier
	 */
	public Panier findByUserAndArticle (User user, Article article);

	
}