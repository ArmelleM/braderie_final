package fr.afpa.braderie.LabBraderie_Spring.controler;

import org.springframework.web.bind.annotation.RestController;

import fr.afpa.braderie.LabBraderie_Spring.beans.User;
import fr.afpa.braderie.LabBraderie_Spring.services.ServiceUser;

import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/user")
public class ControleurUser {
	
	@Autowired
	private ServiceUser serviceUser;  
	private static final Logger logger = Logger.getLogger(ControleurUser.class);

	/**
	 * Appelle la méthode getAll() de la classe ServiceUser qui renvoie une liste
	 * @return la liste de toutes les entrées dans la table t_user
	 */
	@GetMapping("/getall")  
	public Iterable<User> getAll()   
	{  
		logger.info("On passe dans le ControleurUser /getAll()");
		Iterable<User> listeUser = serviceUser.getAll();  
		return listeUser;  
	}
	
	/**
	 * Retourne un Optional User via son id
	 * @param id
	 * @return user(optionnal)
	 */
	@GetMapping(value = "/get{id}")  
	public Optional<User> getByID(@PathVariable("id") Integer id)   
	{  
		Optional<User> user = serviceUser.getByID(id);  
		return user;  
	}
	
	/**
	 * Appelle la méthode add de la classe ServiceUser qui ajoute une entrée dans la table t_user
	 * @param user
	 * @return user ajouté
	 */
	@PutMapping(value = "/add")  
	public User add(@RequestBody User user)   
	{  
		User addUser = serviceUser.add(user);
		return addUser;  
	}
	
	/**
	 * Appelle la méthode delete de la classe ServiceUser qui supprime une entrée dans la table t_user
	 * @param id
	 */
	@DeleteMapping(value = "/delete{id}")
	public void delete(@PathVariable("id") Integer id)
	{
		serviceUser.delete(id);
	}
	
	/**
	 * @param user
	 * @return User mis à jour
	 */
	@PostMapping(value = "/update")
	public User update(@RequestBody User user)
	{
		Integer idUser = user.getIduser();
		User updateUser = serviceUser.update(user, idUser);
		return updateUser;
	}
	
}