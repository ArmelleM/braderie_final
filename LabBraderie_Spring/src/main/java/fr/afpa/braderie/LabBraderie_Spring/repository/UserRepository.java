package fr.afpa.braderie.LabBraderie_Spring.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.afpa.braderie.LabBraderie_Spring.beans.User;

@Repository
public interface UserRepository extends CrudRepository <User, Integer>{
	
	/**
	 * Native Query qui appelle la fonction postgres ISVALIDLOGON(username, password) implémentée dans la base de données. 
	 * Vérification de la validité des éléments de connexion entrés dans la page d'authentification. 
	 * @param login
	 * @param pass
	 * @return true ou false en fonction du résultat
	 */
	@Query(value = "select isvalidlogon(:username, :password);", nativeQuery = true)
    public Boolean isValidLogon(@Param("username") String login, @Param("password") String pass);

	/**
	 * Spring Query qui retourne un User en fonction de son login
	 * @param login
	 * @return user correspondant au login
	 */
	public User findByLogin(String login);

}