package fr.afpa.braderie.LabBraderie_Spring.services;


import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.braderie.LabBraderie_Spring.beans.User;
import fr.afpa.braderie.LabBraderie_Spring.repository.UserRepository;



@Service
public class ServiceUser {

	
	@Autowired
	private UserRepository userRepository;
	
	private static final Logger logger = Logger.getLogger(ServiceUser.class);
	
	public ServiceUser() {
	}

	
	public String youAreHere() {
		return("ServiceUser : ");
	}
	
	/**
	 * Retourne la liste de toutes les entrées dans la table t_user
	 * @return listeUser
	 */
    public Iterable<User> getAll() {
        logger.info("Je passe dans mon getAll() User");
    	Iterable<User> listeUser = userRepository.findAll();
    	for(User user : listeUser) {
    		logger.info(user.toString());
    	}
        return listeUser; 
    }
    
    /**
     * Retourne un Optional User via son id
     * @param id
     * @return Optional.ofNullable(user)
     */
    public Optional<User> getByID(Integer id) {
    	Optional<User> optionalUser = userRepository.findById(id);
    	User user = null;
    	if(optionalUser.isPresent()) {
    		user = optionalUser.get();
    		logger.info(user.toString());
    	}
        logger.info("User : "+user);
        return Optional.ofNullable(user); 
    }
    
    /**
     * Ajoute une entrée dans la table t_user
     * @param user
     * @return addUser
     */
    public User add(User user) {
    	User addUser = userRepository.save(user);
        logger.info("User ajoutée : "+user);
        return addUser; 
    }
    
    /**
     * Supprime une entrée dans la table t_user via son id
     * @param id
     */
    public void delete(Integer id) 
    {
        Optional<User> deleteUser = getByID(id);
        userRepository.deleteById(id);
        logger.info("User supprimée : "+deleteUser);
    }
    
    /**
     * Modifie une entrée dans la table t_user
     * @param user
     * @param id
     * @return updateUser
     */
    public User update(User user, Integer id) {
    	
    	userRepository.deleteById(id);
    	User updateUser = userRepository.save(user);
        logger.info("User mise à jour : "+updateUser);
		return updateUser;
    }
    

    /**
     * Appelle la fonction postgres ISVALIDLOGON(username, password) afin de vérifier les informations de connexion 
     * dans la base de données. 
     * @param login
     * @param pass
     * @return
     */
    public Boolean isValid(String login, String pass) {
    	return userRepository.isValidLogon(login, pass);
    	
    }
    
    
    /**
     * Retourne un User en fonction de son login
     * @param login
     * @return
     */
    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }
    
}
