package fr.afpa.braderie.LabBraderie_Spring;


import java.util.Locale;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@EnableSwagger2
public class App implements WebMvcConfigurer {
	

		private static final Logger logger = Logger.getLogger(App.class);
	
	
	@Bean 
	public LocaleResolver localeResolver() {
	    SessionLocaleResolver localeResolver = new SessionLocaleResolver(); 
	    localeResolver.setDefaultLocale(Locale.FRENCH); 

	    return localeResolver;
	}
	
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
	    LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
	    // Defaults to "locale" if not set
	    localeChangeInterceptor.setParamName("localeData");
	    return localeChangeInterceptor;
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry interceptorRegistry) {
	    interceptorRegistry.addInterceptor(localeChangeInterceptor());
	}

	public static void main(String[] args) {
		
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasenames("lang/message");
		
		logger.info("Application lancée");
		
		
		/**
		 * 
		 * Commandes terminal :
		 * 		mvn tomcat7:run
		 * 		mvn jetty:run
		 * 
		 * 		mvn site
		 * 		mvn javadoc:javadoc
		 * 
		 * 		http://localhost:8080/swagger-ui.html
		 */
		SpringApplication.run(App.class, args);
	}

}
