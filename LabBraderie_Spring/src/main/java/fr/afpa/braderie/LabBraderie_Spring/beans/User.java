/**
 * 
 */
package fr.afpa.braderie.LabBraderie_Spring.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author 31010-83-09
 *
 */
@Entity
@Table(name="t_user")
public class User {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Integer iduser;
	private String login;
	private String pass;
	private Integer nbconnexion;
	
	
	public User() {
		super();

	}
	
	/**
	 * @param login
	 * @param pass
	 */
	public User(String login, String pass) {

		this.login = login;
		this.pass = pass;
	}
	
	/**
	 * @param iduser
	 * @param login
	 * @param pass
	 */
	public User(Integer iduser, String login, String pass) {
		this.iduser = iduser;
		this.login = login;
		this.pass = pass;
	}

	/**
	 * 
	 * @return iduser
	 */
	public Integer getIduser() {
		return iduser;
	}

	/**
	 * 
	 * @param iduser
	 */
	public void setIduser(Integer iduser) {
		this.iduser = iduser;
	}

	/**
	 * 
	 * @return login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * 
	 * @param login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * 
	 * @return pass
	 */
	public String getPass() {
		return pass;
	}

	/**
	 * 
	 * @param pass
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}

	/**
	 * 
	 * @return nbconnexion
	 */
	public Integer getNbconnexion() {
		return nbconnexion;
	}

	/**
	 * 
	 * @param nbconnexion
	 */
	public void setNbconnexion(Integer nbconnexion) {
		this.nbconnexion = nbconnexion;
	}

	@Override
	public String toString() {
		return "User [iduser=" + iduser + ", login=" + login + ", pass=" + pass + ", nbconnexion=" + nbconnexion + "]";
	}

}

