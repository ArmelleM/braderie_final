package fr.afpa.braderie.LabBraderie_Spring.services;


import java.util.Optional;


public interface IService<T>
{  
	Iterable<T> getAll();
	
	Optional<T> getByID(Integer id);
} 