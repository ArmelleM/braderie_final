package fr.afpa.braderie.LabBraderie_Spring.controler;


import org.springframework.web.bind.annotation.RestController;

import fr.afpa.braderie.LabBraderie_Spring.beans.Article;
import fr.afpa.braderie.LabBraderie_Spring.services.ServiceArticle;

import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@RestController
@RequestMapping("/article")
public class ControleurArticle {
	
	@Autowired
	private ServiceArticle serviceArticle;  
	
	private static final Logger logger = Logger.getLogger(ControleurArticle.class);

	/**
	 * Appelle la méthode getAll() du ServiceArticle qui retourne la liste de tous les articles 
	 * @return listeArticle
	 */
	@GetMapping(value = "/getall")  
	public Iterable<Article> getAll()   
	{  
		logger.info("On passe dans le ControleurArticle /getAll()");
		Iterable<Article> listeArticle = serviceArticle.getAll();  
		return listeArticle;  
	}
	
	/**
	 * Appelle la méthode getByID() du ServiceArticle qui retourne un Optional Article via son id
	 * @param id
	 * @return article(optionnal)
	 */
	@GetMapping(value = "/get{id}")  
	public Optional<Article> getByID(@PathVariable("id") Integer id)   
	{  
		Optional<Article> article = serviceArticle.getByID(id);  
		return article;  
	}

	/**
	 * Appelle la méthode add de la classe ServiceArticle qui ajoute un article dans la table t_article 
	 * @param article
	 * @return article ajouté
	 */
	@PutMapping(value = "/add")  
	public Article add(@RequestBody Article article)   
	{  
		Article addArticle = serviceArticle.add(article);
		return addArticle;  
	}
	
	/**
	 * Appelle la méthode delete de la classe ServiceArticle qui supprime un article dans la table t_article 
	 * @param id
	 */
	@DeleteMapping(value = "/delete{id}")
	public void delete(@PathVariable("id") Integer id)
	{
		serviceArticle.delete(id);
	}
	
	/**
	 * Appelle la méthode update de la classe ServiceArticle qui modifie un article dans la table t_article 
	 * @param article
	 * @return article mis à jour
	 */
	@PostMapping(value = "/update")
	public Article update(@RequestBody Article article)
	{
		Integer idArticle = article.getIdarticle();
		Article updateArticle = serviceArticle.update(article, idArticle);
		return updateArticle;
	}

}