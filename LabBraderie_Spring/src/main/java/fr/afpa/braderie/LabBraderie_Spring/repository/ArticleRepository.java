package fr.afpa.braderie.LabBraderie_Spring.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.afpa.braderie.LabBraderie_Spring.beans.Article;


@Repository
public interface ArticleRepository extends CrudRepository <Article, Integer> {
	
	
	
}