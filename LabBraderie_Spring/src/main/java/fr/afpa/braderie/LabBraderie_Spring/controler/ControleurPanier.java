package fr.afpa.braderie.LabBraderie_Spring.controler;

import org.springframework.web.bind.annotation.RestController;
import fr.afpa.braderie.LabBraderie_Spring.beans.Panier;
import fr.afpa.braderie.LabBraderie_Spring.services.ServicePanier;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/panier")
public class ControleurPanier {
	
	@Autowired
	private ServicePanier servicePanier;  
	private static final Logger logger = Logger.getLogger(ControleurPanier.class);

	/**
	 * Appelle la méthode getAll() de la classe ServicePanier qui renvoie une liste 
	 * @return la liste de toutes les entrées dans la table t_panier
	 */
	@GetMapping(value = "/getall")  
	public Iterable<Panier> getAll()   
	{  
		logger.info("On passe dans le ControleurPanier /getAll()");
		Iterable<Panier> listePanier = servicePanier.getAll();  
		return listePanier;  
	}
	
	/**
	 * Appelle la méthode getByID() du ServicePanier qui retourne un Optional Panier via son id
	 * @param id
	 * @return panier(optionnal)
	 */
	@GetMapping(value = "/get{id}")  
	public Optional<Panier> getByID(@PathVariable("id") Integer id)   
	{  
		Optional<Panier> panier = servicePanier.getByID(id);  
		return panier;  
	}

	/**
	 * Appelle la méthode add de la classe ServicePanier qui ajoute une entrée dans la table t_panier
	 * @param panier
	 */
	@PutMapping(value = "/add")  
	public void add(@RequestBody Panier panier)   
	{  
		servicePanier.add(panier);
	}
	
	/**
	 * Appelle la méthode delete de la classe ServicePanier qui supprime une entrée dans la table t_panier 
	 * @param id
	 */
	@DeleteMapping(value = "/delete{id}")
	public void delete(@PathVariable("id") Integer id)
	{
		servicePanier.delete(id);
	}
	
	/**
	 * Appelle la méthode update de la classe ServicePanier qui modifie une entrée dans la table t_panier 
	 * @param panier
	 * @return entrée de la table t_panier mis à jour
	 */
	@PostMapping(value = "/update")
	public Panier update(@RequestBody Panier panier)
	{
		Integer idPanier = panier.getIdpanier();
		Panier updatePanier = servicePanier.update(panier, idPanier);
		return updatePanier;
	}

}