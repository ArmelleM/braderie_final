package fr.afpa.braderie.LabBraderie_Spring.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("fr.afpa.braderie.LabBraderie_Spring")
public class AppConfiguration {
	
//	// Connection à Postgres
//	  @Bean
//	  public DataSource dataSource() {
//	    DriverManagerDataSource datasource = new DriverManagerDataSource();
//	    datasource.setDriverClassName("org.postgresql.Driver");
//	    datasource.setUrl("jdbc:postgresql://localhost:5432/postgres");
//	    datasource.setUsername("postgres");
//	    datasource.setPassword("afpa123");
//	    return datasource;
//	  }
	  
//	@Bean
//	public Logger getLogger() {
//		return LogManager.getLogger(getClass());
//	}
//	
//	@Bean
//	ServiceExterne servExt() {
//		return new ServiceExterne();
//	}

}
