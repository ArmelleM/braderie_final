package fr.afpa.braderie.LabBraderie_Spring.services;


import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.braderie.LabBraderie_Spring.beans.Article;
import fr.afpa.braderie.LabBraderie_Spring.repository.ArticleRepository;


@Service
public class ServiceArticle implements IService<Article>{
	
	@Autowired
	private ArticleRepository articleRepository;
	
	private static final Logger logger = Logger.getLogger(ServiceArticle.class);
	
	public ServiceArticle() {
	}
	
	
	/**
	 * Retourne la liste de tous les articles 
	 * @return listeArticle
	 */
    public Iterable<Article> getAll() {
        logger.info("Je passe dans mon getAll() Article");
    	Iterable<Article> listeArticle = articleRepository.findAll();
    	for(Article article : listeArticle) {
    		logger.info(article.getDescription());
    	}
        return listeArticle; 
    }
    
    /**
     * Retourne un Optional Article via son id
     * @param id
     * @return Optional.ofNullable(article)
     */
    public Optional<Article> getByID(Integer id) {
        logger.info("Je passe dans mon getByID() Article");
    	Optional<Article> optionalArticle = articleRepository.findById(id);
    	Article article = null;
    	if(optionalArticle.isPresent()) {
    		article = optionalArticle.get();
    		logger.info(article.getDescription());
    	}
        return Optional.ofNullable(article); 
    }
    
    /**
     * Ajoute un article dans la table t_article 
     * @param article
     * @return addArticle
     */
    public Article add(Article article) {
    	Article addArticle = articleRepository.save(article);
        logger.info("Article ajoutée : "+article);
        return addArticle; 
    }
    
    /**
     * Supprime un article dans la table t_article 
     * @param id
     */
    public void delete(Integer id) 
    {
        Optional<Article> deleteArticle = getByID(id);
        articleRepository.deleteById(id);
        logger.info("Article supprimée : "+deleteArticle);
    }
    
    /**
     * Modifie un article dans la table t_article 
     * @param article
     * @param id
     * @return updateArticle
     */
    public Article update(Article article, Integer id) {
    	
    	articleRepository.deleteById(id);
    	Article updateArticle = articleRepository.save(article);
        logger.info("Article mise à jour : "+updateArticle);
		return updateArticle;
    } 
    
}
