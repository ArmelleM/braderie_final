package fr.afpa.braderie.LabBraderie_Spring.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.braderie.LabBraderie_Spring.beans.Article;
import fr.afpa.braderie.LabBraderie_Spring.beans.Panier;
import fr.afpa.braderie.LabBraderie_Spring.beans.User;
import fr.afpa.braderie.LabBraderie_Spring.controler.ControleurUser;
import fr.afpa.braderie.LabBraderie_Spring.repository.PanierRepository;

@Service
public class ServicePanier {

	
	@Autowired
	private PanierRepository panierRepository;
	
	@Autowired
	private ServicePanier servicePanier;
	
	private static final Logger logger = Logger.getLogger(ServicePanier.class);

	private static List<Panier> paniers = new ArrayList<Panier>();

	public ServicePanier() {
	}
	
	/**
	 * Retourne la liste de toutes les entrées dans la table t_panier
	 * @return listePanier
	 */
    public Iterable<Panier> getAll() {
        logger.info("Je passe dans mon getAll() Panier");
        Iterable<Panier> listePanier = panierRepository.findAll();
    	for(Panier panier : listePanier) {
    		logger.info(panier.toString());
    	}
        return listePanier; 
    }
    
    /**
     * Retourne un Optional Panier via son id
     * @param id
     * @return Optional.ofNullable(panier)
     */
    public Optional<Panier> getByID(Integer id) {
        logger.info("Je passe dans mon getByID() Panier");
    	Optional<Panier> optionalPanier = panierRepository.findById(id);
    	Panier panier = null;
    	if(optionalPanier.isPresent()) {
    		panier = optionalPanier.get();
    		logger.info(panier.toString());
    	}
        return Optional.ofNullable(panier); 
    }
    
    /**
     * Ajoute une entrée dans la table t_panier
     * @param panier
     */
    public void add(Panier panier) {
    	panierRepository.save(panier);
        logger.info("Panier ajoutée : "+panier);
    }
    
    /**
     * Supprime une entrée dans la table t_panier via son id
     * @param id
     */
    public void delete(Integer id) 
    {
        Optional<Panier> deletePanier = getByID(id);
        panierRepository.deleteById(id);
        logger.info("Panier supprimée : "+deletePanier);
    }
    
    /**
     * Modifie une entrée dans la table t_panier 
     * @param panier
     * @param id
     * @return updatePanier
     */
    public Panier update(Panier panier, Integer id) {
    	
    	panierRepository.deleteById(id);
    	Panier updatePanier = panierRepository.save(panier);
        logger.info("Panier mise à jour : "+updatePanier);
		return updatePanier;
    } 
    
	/**
	 * Retourne la liste de toutes les entrées dans la table t_panier pour un user donné. 
	 * @return listePanier
	 */
    public Iterable<Panier> getAllByUser(User user) {
        logger.info("Je passe dans mon getAllUser() Panier");
        Iterable<Panier> listePanier = panierRepository.findByUser(user);
    	for(Panier panier : listePanier) {
    		logger.info(panier.toString());
    	}
        return listePanier; 
    }
    
    /**
     * Retourne le prix total du panier en calculant la somme de tous les articles du panier
     * @return prixTotalPanier
     */
    public double calculerPrixTotalPanier(HttpSession session) {
    	
        User user = (User) session.getAttribute("user");
		paniers = (List<Panier>) servicePanier.getAllByUser(user);
		Double prixTotalPanier = (double) 0;
		
		for(Panier articlePanier : paniers) {
			Double prixUnitaire = articlePanier.getArticle().getPrixunitaire();
			Integer quantite = articlePanier.getQuantite();
			prixTotalPanier = prixTotalPanier + (prixUnitaire*quantite);
			logger.info("prixTotalPanier de "+articlePanier.getArticle().getDescription()+" = "+prixTotalPanier);
		}
		logger.info("prixTotalPanier = "+prixTotalPanier);
		return prixTotalPanier;
    }
    
    /**
     * Met à jour la quantité chaque fois qu'un utilisateur ajoute un article au panier depuis la liste d'articles
     * @param id
     * @param quantite
     */
    public void updateQuantite(Integer id, Integer quantite) {
        logger.info("updateQuantite");
        Panier panier = getByID(id).get();
        Integer oldQte = panier.getQuantite();
        panier.setQuantite(oldQte + quantite);
        panierRepository.save(panier);
    }
    
    /**
     * Met à jour la quantité quand l'utilisateur souhaite la changer dans le panier
     * @param id
     * @param quantite
     */
    public void updateQuantitePanier(Integer id, Integer quantite) {
        logger.info("updateQuantitePanier");
        Panier panier = getByID(id).get();
        panier.setQuantite(quantite);
        panierRepository.save(panier);
    }
    
    /**
     * Retourne un panier appartenant à un user donné et comprenant un article donné
     * @param user
     * @param article
     * @return
     */
    public Panier findByUserAndArticle(User user, Article article) {
        return panierRepository.findByUserAndArticle(user, article);
    }
    
    /**
     * Supprime l'intégralité du panier de l'utilisateur connecté
     * @param id
     */
    public void deletePanier(HttpSession session) 
    {
        User user = (User) session.getAttribute("user");
		paniers = (List<Panier>) servicePanier.getAllByUser(user);
        panierRepository.deleteAll(paniers);
        logger.info("Panier de "+user.getLogin()+" supprimé.");
    }
}
