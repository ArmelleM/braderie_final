package fr.afpa.braderie.LabBraderie_Spring.controler;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.afpa.braderie.LabBraderie_Spring.App;
import fr.afpa.braderie.LabBraderie_Spring.beans.Article;
import fr.afpa.braderie.LabBraderie_Spring.beans.Panier;
import fr.afpa.braderie.LabBraderie_Spring.beans.User;
import fr.afpa.braderie.LabBraderie_Spring.services.ServiceArticle;
import fr.afpa.braderie.LabBraderie_Spring.services.ServicePanier;
import fr.afpa.braderie.LabBraderie_Spring.services.ServiceUser;

@Controller
public class Controleur {

	@Autowired
	private ServiceArticle serviceArticle;

	@Autowired
	private ServicePanier servicePanier;

	@Autowired
	private ServiceUser serviceUser;
	
	private static final Logger logger = Logger.getLogger(Controleur.class);

	private static List<Article> articles = new ArrayList<Article>();
	private static List<Panier> paniers = new ArrayList<Panier>();
	private static User user = new User();
	/**
	 * 
	 * @return page html index
	 */
	@RequestMapping(value = { "/index" }, method = RequestMethod.GET)
	public String login() {

		return "index";
	}

	/**
	 * Affiche la page d'accueil avec la liste d'articles et le panier de la personne connectée
	 * @param model
	 * @return page braderie
	 */
	@RequestMapping(value = { "/braderie" }, method = RequestMethod.GET)
	public String afficherBraderie(Model model, HttpSession session) {

		user = (User) session.getAttribute("user");

		model.addAttribute("user", user);
		articles = (List<Article>) serviceArticle.getAll();

		paniers = (List<Panier>) servicePanier.getAllByUser(user);
		Double prixTotalPanier = servicePanier.calculerPrixTotalPanier(session);

		model.addAttribute("articles", articles);
		model.addAttribute("paniers", paniers);
		model.addAttribute("prixTotalPanier", prixTotalPanier);

		return "braderie";
	}


	/**
	 * Appelle la methode delete de la classe ServicePanier
	 * qui ajoute un article à t_panier pour l'utilisateur connecté puis actualise la page braderie
	 * @param idarticle
	 * @param quantite
	 * @param session
	 * @return page braderie
	 */
	@RequestMapping(value = { "/add" }, method = RequestMethod.POST)
	public String add(@RequestParam(name="idarticle") Integer idarticle, @RequestParam(name="quantite") Integer quantite, HttpSession session) {

		logger.info("idarticle :"+idarticle+" - quantite :"+quantite);
		user = (User) session.getAttribute("user");
		Article article = serviceArticle.getByID(idarticle).get();
		Panier panier = new Panier(user, article, quantite);

		Panier panierUserBDD=servicePanier.findByUserAndArticle(user, article);
		logger.info(panierUserBDD);
		if(panierUserBDD==null) {
			servicePanier.add(panier);
		}else {
			servicePanier.updateQuantite(panierUserBDD.getIdpanier(), quantite);
		}

		return "redirect:/braderie";   
	}


	/**
	 * Appelle la methode delete de la classe ServicePanier
	 * qui supprime un article de la table t_panier pour l'utilisateur connecté puis actualise la page braderie
	 * @param idpanier
	 * @return page braderie 
	 */
	@RequestMapping(value = { "/delete" }, method = RequestMethod.POST)
	public String delete(@RequestParam(name="idpanier") Integer idpanier) {

		servicePanier.delete(idpanier);
		return "redirect:/braderie";   
	}

	/**
	 * Appelle la methode isValid du ServiceUser
	 * qui vérifie que les informations de connexions entrées par l'utilisateur correspondent à une entrée
	 * de la table t_user et le dirige vers la page braderie. 
	 * Si cela ne correspond pas, l'utilisateur est redirigé vers la page de connexion. 
	 * @param login
	 * @param pass
	 * @param session
	 * @return
	 */
	@RequestMapping(value = { "/login" }, method = RequestMethod.POST)
	public String isValid(@RequestParam String login, @RequestParam String pass, HttpSession session) {

		if(serviceUser.isValid(login, pass)) {

			User user = serviceUser.findByLogin(login);
			session.setAttribute("user", user);
			logger.info("Utilisateur connecté : " + user.getLogin());

			return "redirect:/braderie";   
		}
		else {
			return "redirect:/index";   
		}
	}

	/**
	 * Appelle la méthode updateQuantite du ServicePanier qui met à jour la quantite pour une entrée de la table panier
	 * quand l'utilisateur clique sur le bouton Ajouter.  
	 * @param idpanier
	 * @param quantite
	 * @param session
	 * @return page braderie
	 */
	@RequestMapping(value = { "/update" }, method = RequestMethod.POST)
	public String update(@RequestParam(name="idpanier") Integer idpanier, @RequestParam(name="quantite") Integer quantite, HttpSession session) {

		servicePanier.updateQuantite(idpanier, quantite);

		logger.info("Panier mis à jour ");
		return "redirect:/braderie";   
	}


	/**
	 * Permet de changer la quantite d'un article depuis le panier.
	 * @param idpanier
	 * @param quantite
	 * @param session
	 * @return page braderie actualisée
	 */
	@RequestMapping(value = { "/change" }, method = RequestMethod.POST)
	public String change(@RequestParam(name="idpanier") Integer idpanier, @RequestParam(name="quantite") Integer quantite, HttpSession session) {

		servicePanier.updateQuantitePanier(idpanier, quantite);

		logger.info("Article dans le panier mis à jour ");
		return "redirect:/braderie";   
	}

	/**
	 * Permet de supprimer l'intégralité du panier de l'utilisateur connecté.
	 * @param session
	 * @return page braderie actualisée
	 */
	@RequestMapping(value = { "/deletePanier" }, method = RequestMethod.POST)
	public String deletePanier(HttpSession session) {

		servicePanier.deletePanier(session);
		return "redirect:/braderie";
	}

}
