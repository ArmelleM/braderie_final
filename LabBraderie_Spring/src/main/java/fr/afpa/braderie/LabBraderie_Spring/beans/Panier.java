/**
 * 
 */
package fr.afpa.braderie.LabBraderie_Spring.beans;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @author Armelle
 *
 */
@Entity
@Table(name="t_panier")
public class Panier {
	
	@Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    Integer idpanier;
    Integer quantite;
    @OneToOne ( cascade= {CascadeType.MERGE})
    @JoinColumn( name="iduser" )
    private User user;
    @OneToOne ( cascade= {CascadeType.MERGE})
    @JoinColumn( name="idarticle" )
    private Article article;
	
	
	/**
	 * Constructeur par défaut
	 */
	public Panier() {
		
	}

	/**
	 * @param quantite
	 * @param user
	 * @param article
	 */
	public Panier(User user, Article article, Integer quantite) {
		this.user = user;
		this.article = article;
		this.quantite = quantite;
	}
	
	/**
	 * @param idpanier
	 * @param quantite
	 * @param user
	 * @param article
	 */
	public Panier(Integer idpanier, Integer quantite, User user, Article article) {
		this.idpanier = idpanier;
		this.quantite = quantite;
		this.user = user;
		this.article = article;
	}


	/**
	 * @return the idpanier
	 */
	public Integer getIdpanier() {
		return idpanier;
	}


	/**
	 * @param idpanier the idpanier to set
	 */
	public void setIdpanier(Integer idpanier) {
		this.idpanier = idpanier;
	}


	/**
	 * @return the quantite
	 */
	public Integer getQuantite() {
		return quantite;
	}


	/**
	 * @param quantite the quantite to set
	 */
	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}


	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}


	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}


	/**
	 * @return the article
	 */
	public Article getArticle() {
		return article;
	}


	/**
	 * @param article the article to set
	 */
	public void setArticle(Article article) {
		this.article = article;
	}

	@Override
	public String toString() {
		return "Panier [idpanier=" + idpanier + ", quantite=" + quantite + ", user=" + user + ", article=" + article
				+ "]";
	}

}
