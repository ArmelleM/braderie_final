Avant utilisation de l'Appli : 
-Créer une procédure stockée dans la base de données postgres avec le code suivant : 

   ------> PROCEDURE STOCKEE <------

    CREATE OR REPLACE FUNCTION isValidlogon(IN username varchar, IN password varchar)
RETURNS boolean 
AS $$
BEGIN

IF EXISTS(SELECT * FROM t_user WHERE login = username AND pass = password) THEN
 UPDATE t_user SET nbconnexion = nbconnexion + 1 where login = username and pass= password;
	RETURN 'true';
ELSE
    RETURN 'false';
	
END IF;

END;
$$language plpgsql;

-------------------------------------------------------------------------------------------

Générer la Javadoc : 

- Ouvrir un terminal à partir du projet.
- Saisir la ligne de commande : mvn javadoc:javadoc 
